import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    productList1: [],
    searchResults: [],
    productEdit: undefined
};

const btfrom = createSlice({
    name: "btfrom",
    initialState,
    reducers: {
        addProduct:(state,action) =>{
            state.productList1.push(action.payload)
        },
        editProduct: (state, action) => {
            // action.payload là product
            state.productEdit = action.payload
          },
          deledeProduct: (state, action) => {
            state.productList1 = state.productList1.filter(prd => prd.mssv !== action.payload)
          },
          updateProduct: (state, action) => {
        
         state.productList1 = state.productList1.map((prd) => { 
           if (prd.mssv === action.payload.mssv) { 
             return action.payload 
   
           }
           return prd 
         })
         state.productEdit = undefined // để xác định rằng không có sản phẩm nào đang được chỉnh sửa.
         
       },
       setSearchResults: (state, action) => {
        state.searchResults = action.payload;
      },
     
    
        
    },
});

export const { actions: btfromActions, reducer: btfromReducerToolkit } = btfrom;

