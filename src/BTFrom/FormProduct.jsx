import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { btfromActions } from "../store/Form/slice";

const FormProduct = () => {
  const [formData, setFormData] = useState();
  const dispatch = useDispatch();
  const { productEdit } = useSelector((state) => state.btform);
  console.log();
  const [formErro, setFormErro] = useState();

  
  
  const handleFormdata = (name) => (ev) => {
    const { name, value, minLength, title, max, validity } = ev.target;
    console.log("🚀 ~ file: FormProduct.jsx:16 ~ handleFormdata ~ validity :", validity);
    let mess;
    if (minLength !== -1 && !value.length) {
      // !value.length = true
      mess = ` Vui lòng nhập thông tin ${title}`;
    } else if (value.length < minLength) {
      mess = `Vui lòng nhập tối thiểu ${minLength} ký tự`;
    } else if (value.length > max && max) {
      mess = `Chỉ được nhập tối đa ${max} ký tự`;
    } else if (validity.patternMismatch && name === "email") {
      mess = `Vui lòng nhập đúng định dạng email`;
    } else if (validity.patternMismatch && ["sdt", "mssv"].includes(name)) {
      // pattern là validate dương
      mess = ` Ký tự chỉ là số & không được số âm `;
    }

    setFormData({
      ...formData,
      [name]: mess ? undefined : value,
    });

    setFormErro({
      ...formErro,
      [name]: mess,
    });
  };
  return (
    <div>
      <h3 className=" font-semibold text-center text-xl mt-5  text-blue-800 ">THÔNG TIN SINH VIÊN</h3>
      <form
        onSubmit={(event) => {
          const elements = document.querySelectorAll("input");
          let formErro = {};
          elements.forEach((e) => {
            let mess;
            const { name, value, minLength, title, max, validity } = e;
            if (minLength !== -1 && !value.length) {
              // !value.length = true
              mess = ` Vui lòng nhập thông tin ${title}`;
            } else if (value.length < minLength) {
              mess = `Vui lòng nhập tối thiểu ${minLength} ký tự`;
            } else if (value.length > max && max) {
              mess = `Chỉ được nhập tối đa ${max} ký tự`;
            } else if (validity.patternMismatch && name === "email") {
              mess = `Vui lòng nhập đúng định dạng email`;
            } else if (validity.patternMismatch && ["sdt", "mssv"].includes(name)) {
              // pattern là validate dương
              mess = ` Ký tự chỉ là số & không được số âm `;
            }
            formErro[name] = mess; // ngăn user không nhập gì thì render ra
          });
          event.preventDefault();
          console.log("submit");
          let flag = false;
          for (let key in formErro) {
            if (formErro[key]) {
              flag = true;
              break;
            }
          }
          if (flag) {
            setFormErro(formErro);
            return;
          }
          if (productEdit) {
            dispatch(btfromActions.updateProduct(formData));
          } else {
            dispatch(btfromActions.addProduct(formData));
          }
        }}
        style={{ maxWidth: 1300, marginLeft: "auto", marginRight: "auto" }}
        className=" outline-none text-center rounded-2xl mt-5 custom-shadow"
        noValidate
      >
        <div>
          <input
            name="tim"
            title="tim"
            className="form-input border-blue-400 hover:border-blue-500 border-2 rounded-md outline-none mt-8"
            style={{ width: 1095, height: 40, paddingLeft: "10px" }}
            type="text"
            placeholder="Thanh search chưa làm đc menter ơi "
          />
          <button className="bg-blue-400 hover:bg-blue-500 text-white px-4 py-2 rounded-md ml-1">Tìm</button>
          <div className="grid grid-cols-2 gap-4  ">
            <div style={{ textAlign: "left", marginLeft: "auto", marginRight: "auto", marginTop: 10 }}>
              <p className="mt-5 font-semibold">MSSV</p>
              <input
                onChange={handleFormdata()}
                name="mssv"
                title="mssv"
                minLength={2}
                disabled={!!productEdit}
                max={10}
                value={productEdit?.mssv}
                type="text"
                pattern={"^[0-9]+$"}
                style={{ width: 500, height: 40, paddingLeft: "10px" }}
                className="form-input border-blue-400 border-2 rounded-md outline-none "
              />
              <p className="text-rose-600	">{formErro?.mssv}</p>
            </div>
            <div style={{ textAlign: "left", marginLeft: "auto", marginRight: "auto", marginTop: 29 }}>
              <p className="font-semibold">Họ Tên</p>
              <input
                onChange={handleFormdata()}
                name="name"
                minLength={4}
                type="name"
                max={30}
                style={{ width: 500, height: 40, paddingLeft: "10px" }}
                className="form-input border-blue-400 border-2 rounded-md outline-none "
              />
              <p className="text-rose-600	">{formErro?.name}</p>
            </div>
          </div>
          <div className="grid grid-cols-2 gap-4 mt-4">
            <div style={{ textAlign: "left", marginLeft: "auto", marginRight: "auto" }}>
              <p className="font-semibold">Số điện thoại</p>
              <input
                onChange={handleFormdata()}
                name="sdt"
                title="số điện thoại"
                type="text"
                minLength={6}
                max={12}
                pattern={"^[0-9]+$"}
                style={{ width: 500, height: 40, paddingLeft: "10px" }}
                className="form-input border-blue-400 border-2 rounded-md outline-none"
              />
              <p className="text-rose-600	">{formErro?.sdt}</p>
            </div>
            <div style={{ textAlign: "left", marginLeft: "auto", marginRight: "auto" }}>
              <p className="font-bold">Email</p>
              <input
                onChange={handleFormdata()}
                name="email"
                title="Email"
                type="text"
                pattern="[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,}$"
                style={{ width: 500, height: 40, paddingLeft: "10px" }}
                className="form-input border-blue-400 border-2 rounded-md outline-none"
              />
              <p className="text-rose-600">{formErro?.email}</p>
            </div>
          </div>
          <div className="grid grid-cols-2 gap-4 mt-4">
            <div style={{ marginLeft: 74, marginBottom: 20 }} className=" space-x-3 flex ">
              {productEdit && <button className="bg-blue-500 text-white px-4 py-2 rounded-md">Update</button>}
              {!productEdit && <button className="bg-green-500 text-white px-4 py-2 rounded-md">Create</button>}
            </div>
          </div>
        </div>
      </form>
      ;
    </div>
  );
};

export default FormProduct;
