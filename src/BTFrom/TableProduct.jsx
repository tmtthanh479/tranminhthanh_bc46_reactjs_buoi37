import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { btfromActions } from '../store/Form/slice'

const TableProduct = () => {
  const { productList1 } = useSelector((state) => state.btform)
  const dispath = useDispatch()

  return (
    <div
      style={{ maxWidth: 1300, marginLeft: "auto", marginRight: "auto" }}
      className="p-4 custom-shadow rounded-2xl">
      <div className="overflow-x-auto ">
        <table className="min-w-full ">
          <thead >
            <tr className="bg-gray-100  ">
              <th className="py-2 px-4  ">MSSV</th>
              <th className="py-2 px-4 ">Họ tên</th>
              <th className="py-2 px-4 ">Số Điện Thoại</th>
              <th className="py-2 px-4 ">Email</th>
              <th className="py-2 px-4 "></th>

            </tr>
          </thead>
          <tbody>
            {
              productList1?.map((prd) => (
                <tr className="bg-white text-center ">
                  <td className="py-2 px-4  ">{prd?.mssv}</td>
                  <td className="py-2 px-4 ">{prd?.name}</td>
                  <td className="py-2 px-4">{prd?.sdt}</td>
                  <td className="py-2 px-4">{prd?.email}</td>
                  <td className="py-2 px-4">
                    <button
                      onClick={() => {
                        dispath(btfromActions.deledeProduct(prd.mssv))
                      }}
                      className="bg-green-500 text-white px-3 py-1 rounded-md mr-2">Delete</button>
                    <button
                      onClick={() => {
                        dispath(btfromActions.editProduct(prd))
                      }}
                      className="bg-blue-500 text-white px-3 py-1 rounded-md">Edit</button>
                  </td>
                </tr>
              ))
            }
          </tbody>
        </table>
      </div>
    </div>

  )
}

export default TableProduct