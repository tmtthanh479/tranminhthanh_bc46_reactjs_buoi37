import React from 'react'
import FormProduct from './FormProduct'
import TableProduct from './TableProduct'

const BTFrom = () => {
  return (
    <div>
      <div><FormProduct/></div>
      <div><TableProduct/></div>
    </div>
  )
}

export default BTFrom